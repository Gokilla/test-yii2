<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\modules\wins\models\Wins;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CronController extends Controller
{
    public function update()
    {
        # code...
        $wins = Wins::find()->where(['type' => 1])->andWhere(['status' => 0])->all();
        foreach($wins as $win)
        {
            //здесь лучше работать через Redis чтоб все отправлялось очередями и быстро
            
            //Получаем список победителей со статусом не дошли деньги
            //через релейшен получаем юзеров и записываем к их балансу данные из коллонки Amount

            //Так же необходимо писать статус выполненой работы в логах 
        }
    }
}
