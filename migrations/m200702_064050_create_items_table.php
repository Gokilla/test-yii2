<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%items}}`.
 */
class m200702_064050_create_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
     #Таблица для записи придметов
        $this->createTable('{{%items}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Название'),
            'count' => $this->tinyInteger()->notNull()->comment('кол-во придметов'),
            'is_active' => $this->boolean()->comment('Активность'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%items}}');
    }
}
