<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%wins}}`.
 */
class m200702_081933_create_wins_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%wins}}', [
            'id' => $this->primaryKey(),
            'type' => $this->tinyInteger()->comment('тип выиграша'),
            'amount' => $this->integer()->comment('Кол-во'),
            'user_id' => $this->integer()->comment('Победитель'),
            'item_id' => $this->integer()->comment('Выигрыш'),
            'status' => $this->integer()->comment('Статус'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
        ]);
        $this->addForeignKey(
			'fk-wins-items_id',
			'wins',
			'item_id',
			'items',
			'id',
			'CASCADE'
        );

        $this->addForeignKey(
			'fk-wins-user_id',
			'wins',
			'user_id',
			'user',
			'id',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%wins}}');
    }
}
