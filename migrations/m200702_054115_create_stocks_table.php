<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stocks}}`.
 */
class m200702_054115_create_stocks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        #Таблица склада о наличии типов выигрыша
        $this->createTable('{{%stocks}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Название выигрыша'),
            'count' => $this->integer()->notNull()->comment('Кол-во элементов'),
            'is_active' => $this->boolean()->comment('Активность'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%stocks}}');
    }
}
