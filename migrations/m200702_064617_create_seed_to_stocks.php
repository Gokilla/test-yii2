<?php

use yii\db\Migration;

/**
 * Class m200702_064617_create_seed_to_stocks
 */
class m200702_064617_create_seed_to_stocks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
     #Миграция для инициолизаци стартового кол-во призов
        $this->insert('stocks', [
            'name' => 'Фиат',
            'count' => 12000,
            'is_active' => true,
        ]);
        $this->insert('stocks', [
            'name' => 'Предмет',
            'count' => 54,
            'is_active' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200702_064617_create_seed_to_stocks cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200702_064617_create_seed_to_stocks cannot be reverted.\n";

        return false;
    }
    */
}
