<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%delivery}}`.
 */
class m200702_073912_create_delivery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        #Миграция для таблицы доставки 
        $this->createTable('{{%delivery}}', [
            'id' => $this->primaryKey(),
            'type' => $this->tinyInteger()->notNull()->comment('Тип выигрыша'),
            'items_id' => $this->integer()->comment('id придмета выигрыша'),
            'user_id' => $this->integer()->comment('Победитель'),
            'status' => $this->boolean()->comment('Статус отправки'), 
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания'),
        ]);

        $this->addForeignKey(
			'fk-delivery-items_id',
			'delivery',
			'items_id',
			'items',
			'id',
			'CASCADE'
        );

        $this->addForeignKey(
			'fk-delivery-user_id',
			'delivery',
			'user_id',
			'user',
			'id',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%delivery}}');
    }
}
