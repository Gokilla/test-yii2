<?php

/* @var $this yii\web\View */
/* @var $model app\modules\stocks\models\Stocks */

$this->title = 'Create Stocks';
$this->params['breadcrumbs'][] = ['label' => 'Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

