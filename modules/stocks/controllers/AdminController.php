<?php

namespace app\modules\stocks\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * AdminController implements the CRUD actions for [[app\modules\stocks\models\Stocks]] model.
 * @see app\modules\stocks\models\Stocks
 */
class AdminController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\stocks\models\Stocks';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\stocks\models\search\StocksSearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\stocks\models\Stocks',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
