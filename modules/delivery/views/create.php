<?php

/* @var $this yii\web\View */
/* @var $model app\modules\delivery\models\Delivery */

$this->title = 'Create Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

