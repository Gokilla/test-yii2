<?php

namespace app\modules\delivery\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * AdminController implements the CRUD actions for [[app\modules\delivery\models\Delivery]] model.
 * @see app\modules\delivery\models\Delivery
 */
class AdminController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\delivery\models\Delivery';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\delivery\models\search\DeliverySearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\delivery\models\Delivery',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
