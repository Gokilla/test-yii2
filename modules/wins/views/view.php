<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\wins\models\Wins */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'amount',
            'user_id',
            'item_id',
            'status',
            'created_at',
        ],
    ]) ?>
    </div>
</div>