<?php

/* @var $this yii\web\View */
/* @var $model app\modules\wins\models\Wins */

$this->title = 'Update Wins: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


