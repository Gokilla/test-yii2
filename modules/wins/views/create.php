<?php

/* @var $this yii\web\View */
/* @var $model app\modules\wins\models\Wins */

$this->title = 'Create Wins';
$this->params['breadcrumbs'][] = ['label' => 'Wins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

