<?php

use dosamigos\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wins\models\search\WinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wins';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'type',
        'amount',
        'user_id',
        'item_id',
        // 'status',
        // 'created_at',

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
