<?php

namespace app\modules\wins\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * AdminController implements the CRUD actions for [[app\modules\wins\models\Wins]] model.
 * @see app\modules\wins\models\Wins
 */
class AdminController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\wins\models\Wins';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\wins\models\search\WinSearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\wins\models\Wins',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
