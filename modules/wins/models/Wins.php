<?php

namespace app\modules\wins\models;

use app\modules\delivery\models\Delivery;
use app\modules\items\models\Items;
use app\modules\stocks\models\Stocks;
use Yii;
use yii\rbac\Item;

/**
 * This is the model class for table "Wins".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $amount
 * @property integer $user_id
 * @property integer $item_id
 * @property integer $status
 * @property string $created_at
 */
class Wins extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Wins';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'amount', 'user_id', 'item_id', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Items::class, 'targetAttribute' => ['item_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'amount' => 'Amount',
            'user_id' => 'User ID',
            'item_id' => 'Item ID',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Метод для генирации приза
     */
    public static function Roll()
    {
        $type = rand(1,3);
        switch ($type) {
            #Если выпало фиат
            case '1':
                $stock = Stocks::find()->where(['id' => 1])->one();
                if($stock->count == 0){
                    #Зачесляем бонуссные деньги ибо обычные кончились C:
                }else{
                    #Рандоми кол-во выигрышных денег чтоб не все потратить - 1000 TODO: Дать в админке устанавливать диапозон
                    $sum = rand(1, $stock->count - 1000);

                    //Вычетаем деньги которые выграл пользователь с обшего пула
                    $stock->count = $stock->count - $sum;
                    $stock->save();

                    //Записываем выигрыш
                    $model = new Wins();
                    $model->type = 1;
                    $model->amount = $sum;
                    $model->user_id = 1; //сюда передаем id победителя
                    $model->status = 1; //успешно отправляем денюжки в лучшем случаае  TODO: оброботчик через экспешн
                    $model->save();
                }
            break;

            case '2':
                $stock = Stocks::find()->where(['id' => 2])->one();
                if($stock->count == 0){
                    #Зачесляем бонуссные деньги ибо призы кончились C:
                }else{
                    #получение рандомного придмета из наличия
                    $item = Items::find()->where(['IS NOT', 'count', 0])->andWhere(['is_active' => 1])->orderBy(['rand()'])->limit(1);
                    //Убираем 1 придемет как мы его уже разыграли
                    $item->count -= 1;
                    $item->save();
                    
                    #Запись что нужно отправить выигрыш
                    $delivery = new Delivery();
                    $delivery->type = 2; //запись типа выйгрыша
                    $delivery->items_id = $item->id ; //так-как это деньги и такого придмета у нас нет
                    $delivery->user_id = 1; //сюда пишем id_usera которы начал розыгрыш
                    $delivery->status = 0; // записываем что нужно отправить выигрыш 
                    $delivery->save();

                    //Убераем выигранный предмет со склада
                    $stock->count = $stock->count - 1; 
                    $stock->save();
                    
                     //Записываем выигрыш
                     $model = new Wins();
                     $model->type = 2;
                     $model->amount = 1;
                     $model->user_id = 1; //сюда передаем id победителя
                     $model->item_id = $item->id; // Выигрыш
                     $model->status = 1; // То что запись создана в доставке
                     $model->save();
                }
            break;

            case '3':
                #Запись бонусов на счет пользователя 
            break;
        }
    }

}
